//#define DEBUG

#ifndef UNICODE
#define UNICODE
#endif

#ifndef _UNICODE
#define _UNICODE
#endif

#undef _DEBUG

#include <Windows.h>
#include <string>

// returns true if fullString ends with ending string
bool hasEnding(std::wstring const &fullString, std::wstring const &ending);

// return widestring of a string
std::wstring s2ws(const std::string& s);

// tries to start an exe with parameters.
size_t ExecuteProcess(std::wstring FullPathToExe, std::wstring Parameters, size_t SecondsToWait);

// hide the console window
void Stealth();
