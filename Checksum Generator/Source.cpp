#include "../Commons.h"

#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

// requires #include <algorithm>
bool CompareStr(wstring stringA, wstring stringB)
{
	transform(stringA.begin(), stringA.end(), stringA.begin(), toupper);
	transform(stringB.begin(), stringB.end(), stringB.begin(), toupper);

	return (stringA == stringB);
}

int main()
{
	wstring installer, library, checksum_file, temp;
	bool everythingOk = false;

	checksum_file = L"Checksums " + std::to_wstring(MAJOR) + L"." + std::to_wstring(MINOR) + L".txt";

#ifdef DEBUG
	std::wcout << L"WARNING! CURRENT BUILD HAS DEBUG TURNED ON!! \n\n";
#else
	std::wcout << L"BUILD WITH MAJOR: " << MAJOR << " MINOR: " << MINOR << "\n\n";
#endif

	do{
		std::wcout << L"Enter The Dynamic Link Library Name: \n";
		wcin >> library;

		if (CompareStr(library, L"exit")) return 0;
		if (CompareStr(library, L"quit")) return 0;

		if (!PathFileExists(library.c_str()))
		{
			std::wcout << library << L" NOT Found! Try again!\n\n";
			everythingOk = false;
		}
		else
		{
			std::wcout << library << L" OK\n";
			everythingOk = true;
		}

	} while (everythingOk == false);

	std::wcout << L"\n";

	do{
		std::wcout << L"Enter The Installer Name: \n";
		wcin >> installer;

		if (CompareStr(installer, L"exit")) return 0;
		if (CompareStr(installer, L"quit")) return 0;

		if (!PathFileExists(installer.c_str()))
		{
			std::wcout << installer << L" NOT Found! Try again!\n\n";
			everythingOk = false;
		}
		else
		{
			std::wcout << installer << L" OK\n";
			everythingOk = true;
		}

	} while (everythingOk == false);

	wofstream hChecksum(checksum_file, std::wofstream::app);

	temp = L"Library: " + library + L"\ndllcs=\n";
	temp += CalculateChecksum(library.c_str());
	temp += L"\n";
	hChecksum << temp;

	temp = L"\nInstaller: " + installer + L"\nexecs=\n";
	temp += CalculateChecksum(installer.c_str());
	temp += L"\n";
	hChecksum << temp;

	temp = L"=================================\n";
	hChecksum << temp;

	hChecksum.flush();
	hChecksum.close();

	std::wcout << "\nAll done! See " << checksum_file << " for the generated checksums.";

	std::wcout << endl << endl;
	std::system("pause");

	return 0;
}