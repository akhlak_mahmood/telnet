#define UNICODE

#include <Windows.h>
#include <string>

#ifndef LG_HPP
#define LG_HPP

#include <fstream>
#include <iostream>

class Lg {

public:
	// If you can´t/dont-want-to use C++11, remove the "class" word after enum
	enum class Type { F, E, W, I };
	enum class Level { NONE, F, E, W, I, ALL };

	// ctor (remove parameters if you don´t need them)
	explicit Lg(WCHAR *introMsg, const Level lvl)
		: numWarnings(0U),
		numErrors(0U)
	{
		fileSet = false;
		log_level = lvl;
		introMessage = introMsg;
		current_level = Level::ALL;
	}


	// dtor
	~Lg() {

		if (fileSet && myFile.is_open()) {
			if (log_level != Level::NONE)
			{
				myFile << std::endl;

				// Report number of errors and warnings
				myFile << numWarnings << L" warnings, " << numErrors << L" errors" << std::endl;
				myFile << L"[End]" << std::endl;

				myFile.close();
			}
		} // if

	}


	// Overload << operator using log level to set new level
	friend Lg &operator << (Lg &logger, const Level newLvl) {
		logger.log_level = newLvl;
		return logger;
	}

	// Overload << operator using log type
	friend Lg &operator << (Lg &logger, const Type l_type) {
		if (logger.fileSet && logger.log_level != Level::NONE)
		{
			logger.current_level = Lg::Level::ALL;

			switch (l_type) {

			case Lg::Type::F:
				logger.current_level = Lg::Level::F;
				logger.myFile << std::endl << L"[FATAL]: ";
				++logger.numErrors;
				break;

			case Lg::Type::E:
				logger.current_level = Lg::Level::E;
				if (logger.log_level > Lg::Level::F)
					logger.myFile << std::endl << L"[ERROR]: ";
				++logger.numErrors;
				break;

			case Lg::Type::W:
				logger.current_level = Lg::Level::W;
				if (logger.log_level > Lg::Level::E)
					logger.myFile << std::endl << L"[WARN]: ";
				++logger.numWarnings;
				break;

			default:
				if (l_type == Lg::Type::I)
					logger.current_level = Lg::Level::I;
				if (logger.log_level > Lg::Level::W)
					logger.myFile << std::endl << L"[INFO]: ";
				break;
			} // sw

		}
		return logger;

	}


	// Overload << operator using C style strings
	// No need for std::string objects here
	friend Lg &operator << (Lg &logger, std::wstring const text) {
		
		if (logger.fileSet && logger.log_level != Level::NONE)
		{
			if (logger.current_level <= logger.log_level)
			{
				logger.myFile << text; // << std::endl;
				logger.myFile.flush();

				if (logger.log_level == Lg::Level::ALL)
					std::wcout << text << std::endl;
			}
			logger.current_level = Lg::Level::ALL;
		}

		return logger;
	}

	// override >> operator to set the log file name.
	friend Lg &operator >> (Lg &logger, const WCHAR *fname){
		if (!logger.fileSet && logger.log_level != Level::NONE)
		{
			logger.fileSet = true;
			logger.myFile.open(fname);

			if (logger.myFile.is_open()) {
				logger.myFile << L"[BEGIN]" << std::endl;
				logger.myFile << logger.introMessage << L" level: " << (int)logger.log_level << std::endl << std::endl;
			}
		}

		return logger;
	}

	// Make it Non Copyable (or you can inherit from sf::NonCopyable if you want)
	Lg(const Lg &) = delete;
	Lg &operator= (const Lg &) = delete;



private:

	std::wofstream          myFile;				// log file handler
	bool					fileSet;			// if the log file name has been given

	WCHAR					*introMessage;		// intro message to write at the first
	unsigned int            numWarnings;
	unsigned int            numErrors;
	Level					log_level;
	Level					current_level;

}; // class end


#endif // LG_HPP

///////////////////////////////////////////////// USAGE ////////////////////////////////////////////////////////
/*

// include the logger if using from a different file. DON'T FORGET TO COMMENT THIS USAGE THEN!!
// #include "Logger.h"

// a global Log
Lg Log(L"Intro message", Lg::Level::ALL);

int main()
{
	// path of the log file
	Log >> L"test.log";

	// now we start logging
	Log << L"This is a simple message.";		// verbose
	Log << Lg::Type::E << L"This is an error.";

	// we can change the log level, for example set it to log warnings and above
	Log << Lg::Level::W;

	// Now, as info is lower than warning, it won't be logged.
	Log << Lg::Type::I << L"This message won't be logged, as its an info.";

	// Levels are:	ALL				-		(verbose) Log everything (Normal debug message)
	//				Info			-		Information (Something should be noted)
	//				Warings (W)		-		Warning (Something suspicious happened)
	//				Error (E)		-		Error (Something bad happened, but application can continue execution)
	//				Fatal (F)		-		Fatal (A fatal error happened, application must terminate)

	Log << Lg::Type::F << L"This will be printed as fatal is higher than warning.";

	// if the log level is set to Lg::Level::ALL it will print the messages to the console.
	
	// resetting level to ALL
	Log << Lg::Level::ALL;
	Log << L"This will be printed to the console.";
	Log << Lg::Type::I << L"And this too.............";

	Log << L"Ba bye!";

	system("pause");
	return 0;
}

*/
/////////////////////////////////////////////// USAGE ENDS ////////////////////////////////////////////////////