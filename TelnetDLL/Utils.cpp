#include "Utils.h"

TCHAR   szMoveDiskName[33];

bool hasEnding(std::wstring const &fullString, std::wstring const &ending)
{
	if (fullString.length() >= ending.length()) {
		return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
	}
	else {
		return false;
	}
}

std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

std::string ws2s(const std::wstring &wstr)
{
	// Convert a Unicode string to an ASCII string
	std::string strTo;
	char *szTo = new char[wstr.length() + 1];
	szTo[wstr.size()] = '\0';
	WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, szTo, (int)wstr.length(), NULL, NULL);
	strTo = szTo;
	delete[] szTo;
	return strTo;
}

bool PostFile(std::wstring wPage, LPSTR filename, std::wstring wFile)
{
	CURL *curl;
	CURLcode res;

	struct curl_httppost *formpost = NULL;
	struct curl_httppost *lastptr = NULL;
	struct curl_slist *headerlist = NULL;
	static const char buf[] = "Expect:";

	curl_global_init(CURL_GLOBAL_ALL);

	/* Fill in the file upload field */
	curl_formadd(&formpost,
		&lastptr,
		CURLFORM_COPYNAME, filename,
		CURLFORM_FILE, ws2s(wFile).c_str(),
		CURLFORM_END);

	curl = curl_easy_init();
	/* initalize custom header list (stating that Expect: 100-continue is not
	wanted */
	headerlist = curl_slist_append(headerlist, buf);
	if (curl) {
		bool ret = false;
		/* what URL that receives this POST */
		curl_easy_setopt(curl, CURLOPT_URL, ws2s(wPage).c_str());
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if (res == CURLE_OK) ret = true;

		/* always cleanup */
		curl_easy_cleanup(curl);

		/* then cleanup the formpost chain */
		curl_formfree(formpost);
		/* free slist */
		curl_slist_free_all(headerlist);

		return ret;
	}
	else return false;
}


size_t ExecuteProcess(std::wstring FullPathToExe, std::wstring Parameters, size_t SecondsToWait)
{
	size_t iMyCounter = 0, iReturnVal = 0, iPos = 0;
	DWORD dwExitCode = 0;
	std::wstring sTempStr = L"";

	/* - NOTE - You should check here to see if the exe even exists */

	/* Add a space to the beginning of the Parameters */
	if (Parameters.size() != 0)
	{
		if (Parameters[0] != L' ')
		{
			Parameters.insert(0, L" ");
		}
	}

	/* The first parameter needs to be the exe itself */
	sTempStr = FullPathToExe;
	iPos = sTempStr.find_last_of(L"\\");
	sTempStr.erase(0, iPos + 1);
	Parameters = sTempStr.append(Parameters);

	/* CreateProcessW can modify Parameters thus we allocate needed memory */
	wchar_t * pwszParam = new wchar_t[Parameters.size() + 1];
	if (pwszParam == 0)
	{
		return 1;
	}
	const wchar_t* pchrTemp = Parameters.c_str();
	wcscpy_s(pwszParam, Parameters.size() + 1, pchrTemp);

	/* CreateProcess API initialization */
	STARTUPINFOW siStartupInfo;
	PROCESS_INFORMATION piProcessInfo;
	memset(&siStartupInfo, 0, sizeof(siStartupInfo));
	memset(&piProcessInfo, 0, sizeof(piProcessInfo));

	siStartupInfo.cb = sizeof(siStartupInfo);
	siStartupInfo.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	siStartupInfo.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	siStartupInfo.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	siStartupInfo.hStdError = GetStdHandle(STD_ERROR_HANDLE);
	siStartupInfo.wShowWindow = SW_HIDE;

	if (CreateProcessW(const_cast<LPCWSTR>(FullPathToExe.c_str()),
		pwszParam, 0, 0, false,
		CREATE_DEFAULT_ERROR_MODE, 0, 0,
		&siStartupInfo, &piProcessInfo) != false)
	{
		/* Watch the process. */
		dwExitCode = WaitForSingleObject(piProcessInfo.hProcess, (SecondsToWait * 1000));
	}
	else
	{
		/* CreateProcess failed */
		iReturnVal = GetLastError();
	}

	/* Free memory */
	delete[]pwszParam;
	pwszParam = 0;

	/* Release handles */
	CloseHandle(piProcessInfo.hProcess);
	CloseHandle(piProcessInfo.hThread);

	return iReturnVal;
}

// Returns a string of the format "PC: computer name User: Current user name"
std::wstring GetSystemInfo()
{
	WCHAR  infoBuf[INFO_BUFFER_SIZE];
	DWORD  bufCharCount = INFO_BUFFER_SIZE;

	// Get the name of the computer. 
	bufCharCount = INFO_BUFFER_SIZE;
	if (GetComputerName(infoBuf, &bufCharCount))
		return std::wstring(infoBuf);
	else return L"Unknown PC";
}


char chFirstDriveFromMask(ULONG unitmask)
{
	char i;
	for (i = 0; i < 26; ++i)
	{
		if (unitmask & 0x1)
			break;
		unitmask = unitmask >> 1;
	}
	return (i + 'A');
}

TCHAR *GetUSBDiskLetters()
{
	// USB letters' container
	TCHAR	szDrvName[33];

	int k = 0;
	DWORD MaxDriveSet;
	DWORD drive;

	for (k = 0; k<26; k++)
		szMoveDiskName[k] = '\0';
	k = 1;

	// Get available drives we can monitor
	MaxDriveSet = GetLogicalDrives();

	for (drive = 0; drive < 32; ++drive)
	{
		if (MaxDriveSet & (1 << drive))
		{
			DWORD temp = 1 << drive;
#pragma warning (disable : 4996)
			swprintf(szDrvName, 33, L"%c:\\", 'A' + drive);
			switch (GetDriveType(szDrvName))
			{
			case DRIVE_REMOVABLE:	// The drive can be removed from the drive.
				szMoveDiskName[k] = chFirstDriveFromMask(temp);
				szMoveDiskName[0] = k;
				k++;
				break;
			case DRIVE_CDROM:		// The drive is a CD-ROM drive.
				break;
			}
		}
	}

	return szMoveDiskName;
}

HRESULT CreateLink(LPCWSTR lpszPathObj, LPCWSTR lpszPathLink, LPCWSTR lpszDesc, LPCWSTR lpszWorkingDir)
{
	//#include <WinNls.h>
	//#include <shobjidl.h>
	//#include <objbase.h>
	//#include <ObjIdl.h>
	//#include <shlguid.h>

	HRESULT hres;
	IShellLink* psl;

	hres = CoInitialize(NULL);
	if (SUCCEEDED(hres))
	{
		hres = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLink, (LPVOID*)&psl);
		if (SUCCEEDED(hres))
		{
			IPersistFile* ppf;
			psl->SetPath(lpszPathObj);
			psl->SetDescription(lpszDesc);
			psl->SetWorkingDirectory(lpszWorkingDir);
			hres = psl->QueryInterface(IID_IPersistFile, (LPVOID*)&ppf);

			if (SUCCEEDED(hres))
			{
				hres = ppf->Save(lpszPathLink, TRUE);
				ppf->Release();
			}
			psl->Release();
		}
	}
	return hres;
}
